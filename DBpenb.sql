-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2020 at 05:54 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penbcafe`
--

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `Card_ID` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `Cus_No` int(10) NOT NULL,
  `Phone` varchar(10) NOT NULL,
  `Card_ID` int(3) NOT NULL,
  `P_Date` date DEFAULT NULL,
  `Login_Time` datetime NOT NULL,
  `Logout_Time` datetime DEFAULT NULL,
  `Emp_ID` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `Emp_ID` varchar(6) NOT NULL,
  `Fname` varchar(50) NOT NULL,
  `Lname` varchar(50) NOT NULL,
  `Phone` varchar(10) NOT NULL,
  `Gender` varchar(1) NOT NULL,
  `Address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `internet_cafe`
--

CREATE TABLE `internet_cafe` (
  `Commercial_ID` varchar(15) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Location` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

CREATE TABLE `summary` (
  `Emp_ID` varchar(6) NOT NULL,
  `S_Date` date NOT NULL,
  `Commercial_ID` varchar(15) NOT NULL,
  `Total_Income` int(11) NOT NULL DEFAULT 0,
  `Total_Expense` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `work_on`
--

CREATE TABLE `work_on` (
  `Emp_ID` varchar(6) NOT NULL,
  `W_Date` date NOT NULL,
  `Commercial_ID` varchar(15) NOT NULL,
  `Emp_Start` datetime NOT NULL,
  `Emp_Stop` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`Card_ID`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`Cus_No`),
  ADD KEY `Card_ID` (`Card_ID`),
  ADD KEY `Emp_ID` (`Emp_ID`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`Emp_ID`);

--
-- Indexes for table `internet_cafe`
--
ALTER TABLE `internet_cafe`
  ADD PRIMARY KEY (`Commercial_ID`);

--
-- Indexes for table `summary`
--
ALTER TABLE `summary`
  ADD PRIMARY KEY (`Emp_ID`,`S_Date`),
  ADD KEY `Commercial_ID` (`Commercial_ID`);

--
-- Indexes for table `work_on`
--
ALTER TABLE `work_on`
  ADD PRIMARY KEY (`Emp_ID`,`W_Date`),
  ADD KEY `Commercial_ID` (`Commercial_ID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`Card_ID`) REFERENCES `card` (`card_ID`),
  ADD CONSTRAINT `customer_ibfk_2` FOREIGN KEY (`Emp_ID`) REFERENCES `employee` (`Emp_ID`);

--
-- Constraints for table `summary`
--
ALTER TABLE `summary`
  ADD CONSTRAINT `summary_ibfk_1` FOREIGN KEY (`Emp_ID`) REFERENCES `employee` (`Emp_ID`),
  ADD CONSTRAINT `summary_ibfk_2` FOREIGN KEY (`Commercial_ID`) REFERENCES `internet_cafe` (`Commercial_ID`);

--
-- Constraints for table `work_on`
--
ALTER TABLE `work_on`
  ADD CONSTRAINT `work_on_ibfk_1` FOREIGN KEY (`Emp_ID`) REFERENCES `employee` (`Emp_ID`),
  ADD CONSTRAINT `work_on_ibfk_2` FOREIGN KEY (`Commercial_ID`) REFERENCES `internet_cafe` (`Commercial_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
